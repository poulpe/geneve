# -*- coding: utf-8 -*-
from constants import TILE_SIZE, SCREEN_SIZE, game_globals
from random import randint, choice
from movable import Movable
from pyglet import image, graphics, sprite, clock


img_dead = image.load('data/enemy_dead.png')
frames = ('data/medic_ok1.png',
          'data/medic_ok2.png',
          'data/medic_ok3.png',
          'data/medic_ok2.png')
anim_running = [image.load(img) for img in frames]
anim_running_flipped = [image.load(img) for img in frames]
for frame in anim_running_flipped:
    frame.anchor_x = frame.width

go_right = image.Animation.from_image_sequence(anim_running, 0.12)

go_left = image.Animation.from_image_sequence(anim_running_flipped,
                                              0.12).get_transform(flip_x=True)

active_medics = []
dead_medics = []


class Medic(Movable):
    speed = 50

    def __init__(self, batch, wounded):
        self.batch = batch
        # 16 is self.sprite.width but this is not initialized yet
        self.pos_x = randint(TILE_SIZE,
                             SCREEN_SIZE - TILE_SIZE - 16)
        # 22 is self.sprite.height but this is not initialized yet
        self.pos_y = SCREEN_SIZE + 22

        self.wounded = wounded

        self.calculate_vector(self.pos_x, self.pos_y)

        if self.speed_x < 0:
            animation = go_left
        else:
            animation = go_right

        self.dead = False

        super(Medic, self).__init__(animation,
                                    self.move,
                                    self.batch)
        active_medics.append(self)

    def move(self, _delta):
        dist_x, dist_y = self.get_distances(self.sprite.x, self.sprite.y)
        dist = (dist_x ** 2 + dist_y ** 2) ** .5

        d_x = self.speed_x * self.delta
        d_y = self.speed_y * self.delta
        speed_vect = (d_x ** 2 + d_y ** 2) ** .5

        if dist > speed_vect:
            self.sprite.x += d_x
            self.sprite.y += d_y
        else:
            if self.wounded:
                self.sprite.x = self.wounded.sprite.x
                self.sprite.y = self.wounded.sprite.y
                self.stop_update()
                self.start_healing(self.wounded)
            else:
                self.kill(None)

    def start_healing(self, wounded):
        clock.schedule_once(self.finish_healing, 5, wounded)

    def finish_healing(self, _dt, wounded):
        wounded.heal()
        stunts = [enemy for enemy in wounded.active_enemies if enemy.stunt_by]
        release_medic(wounded, stunts)
        wounded.medics = []

    def get_distances(self, pos_x, pos_y):
        if self.wounded:
            dest_x = self.wounded.sprite.x
            dest_y = self.wounded.sprite.y
        else:
            dest_x = self.sprite.x
            dest_y = 900

        return (dest_x - pos_x, dest_y - pos_y)

    def calculate_vector(self, pos_x, pos_y):
        dist_x, dist_y = self.get_distances(pos_x, pos_y)
        dist = (dist_x ** 2 + dist_y ** 2) ** .5

        self.speed_x = self.speed * dist_x / dist
        self.speed_y = self.speed * dist_y / dist

    def kill(self, rocket):
        self.dead = True
        if self.wounded:
            self.wounded.medics.remove(self)
            self.wounded = None
        dead_medics.append(self)
        active_medics.remove(self)
        self.sprite.delete()
        self.sprite = sprite.Sprite(img_dead,
                                    batch=self.batch,
                                    x=self.sprite.x,
                                    y=self.sprite.y)
        self.stop_update()
        clock.unschedule(self.finish_healing)
        if rocket:
            # this means it is a rocket that killed us
            game_globals["points"].hit()

medics_batch = graphics.Batch()


def call_medic(wounded):
    m = Medic(medics_batch, wounded)
    return m


def release_medic(wounded, other_stunt_enemies):
    for medic in wounded.medics:
        old_wounded = medic.wounded
        if other_stunt_enemies:
            medic.wounded = choice(other_stunt_enemies)
            medic.wounded.medics.append(medic)
        else:
            medic.wounded = None
        medic.calculate_vector(medic.sprite.x, medic.sprite.y)
        clock.unschedule(medic.cb)  # in case it was already running
        clock.schedule_interval(medic.cb, medic.delta)
        clock.unschedule(medic.finish_healing)
