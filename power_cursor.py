# -*- coding: utf-8 -*-
from constants import SCREEN_SIZE
from pyglet import image, sprite, clock

slot_img = image.load('data/power_slot.png')
power_img = image.load('data/power.png')
cursor_img = image.load('data/power_cursor.png')


class PowerCursor(object):

    slot_pos_x = SCREEN_SIZE - 288
    power_pos_x = slot_pos_x + 18
    cursor_pos_x = slot_pos_x + 18 - 2
    slot_pos_y = 0
    power_pos_y = slot_pos_y + 5
    cursor_pos_y = slot_pos_y

    def __init__(self, tank):
        self.slot_sprite = sprite.Sprite(slot_img,
                                         x=self.slot_pos_x,
                                         y=self.slot_pos_y)
        self.power_sprite = sprite.Sprite(power_img,
                                          x=self.power_pos_x,
                                          y=self.power_pos_y)
        self.cursor_sprite = sprite.Sprite(cursor_img,
                                           x=self.cursor_pos_x,
                                           y=self.cursor_pos_y)
        self.tank = tank
        self.power_amount = 0

    def draw(self):
        self.slot_sprite.draw()
        self.power_sprite.draw()
        self.cursor_sprite.draw()

    def grow(self, _delta):
        self.power_amount += 110 / 60.
        self.power_amount = min(221, self.power_amount)
        self.power_sprite.x = self.power_pos_x + self.power_amount

    def press(self):
        clock.schedule_interval(self.grow, 1 / 60.)
        self.power_sprite.x = self.power_pos_x

    def release(self):
        clock.unschedule(self.grow)
        self.tank.fire(self.power_amount * 3)
        self.power_amount = 0
        self.cursor_sprite.x = self.power_sprite.x - 2
