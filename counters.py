# -*- coding: utf-8 -*-

from pyglet import text, sprite, image, graphics
from pyglet.gl import gl


class PointsCounter(object):

    def __init__(self):
        self.red_cross = sprite.Sprite(image.load("data/red_cross.png"),
                                       x=2,
                                       y=723, )
        self.hits = 0
        self.label = text.Label(":0",
                                font_name="Times New Roman",
                                font_size=24,
                                x=68,
                                y=735, )

    def draw(self):
        self.red_cross.draw()
        self.label.draw()

    def hit(self):
        self.hits += 1
        self.label.text = ":%d" % self.hits


class LifeCounter(object):

    def __init__(self):
        self.label = text.Label("armor:",
                                font_name="Times New Roman",
                                font_size=24,
                                x=4,
                                y=8, )
        self.life = 100

    def hit(self, how_much):
        self.life -= how_much
        if (self.life <= 0):
            self.life = 0
        return self.life

    def draw(self):
        self.label.draw()
        w = 250 * self.life / 100

        if self.life > 66:
            gl.glColor3f(0, .75, 0)
        elif self.life > 33:
            gl.glColor3f(.75, .75, 0)
        else:
            gl.glColor3f(.75, 0, 0)

        graphics.draw(4, gl.GL_QUADS, ('v2f', [100, 8,
                                               100 + w, 8,
                                               100 + w, 24,
                                               100, 24]))
