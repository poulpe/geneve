# -*- coding: utf-8 -*-
from constants import TILE_SIZE, SCREEN_SIZE
from movable import Movable
from pyglet import image, sprite, graphics
from medic import active_medics

rocket_img = image.load('data/rocket.png')
impact_img = image.load('data/impact.png')


class Rocket(Movable):
    pos_y = 64
    ground_friction = 256
    background = graphics.OrderedGroup(0)
    foreground = graphics.OrderedGroup(1)

    def __init__(self,
                 initial_x,
                 batch,
                 initial_speed=[0, 256],
                 active_enemies=[]):
        self.pos_x = initial_x
        self.start_y = self.pos_y
        self.impact_y = self.pos_y + initial_speed[1]
        self.x_speed = initial_speed[0]
        self.y_speed = initial_speed[1]
        self.initial_speed = initial_speed[:]
        self.impact_sprite = None
        self.batch = batch
        self.active_enemies = active_enemies
        super(Rocket, self).__init__(rocket_img,
                                     self.move,
                                     batch,
                                     self.foreground)

    def check_collision(self, contact_func):
        for enemy in self.active_enemies:
            sq_distance = (enemy.sprite.x - self.sprite.x)**2 + \
                          (enemy.sprite.y - self.sprite.y)**2
            if sq_distance <= 384:
                getattr(enemy, contact_func)(self)

        for medic in active_medics:
            sq_distance = (medic.sprite.x - self.sprite.x)**2 + \
                          (medic.sprite.y - self.sprite.y)**2
            if sq_distance <= 384:
                medic.kill(self)

    def move(self, _delta):
        if self.sprite.y > self.impact_y:
            contact_func = "stun"
            if not self.impact_sprite:
                # first ground landing
                self.impact_sprite = sprite.Sprite(impact_img,
                                                   batch=self.batch,
                                                   group=self.background,
                                                   x=self.sprite.x,
                                                   y=self.sprite.y)
                contact_func = "kill"

            self.check_collision(contact_func)

            # we have touched the ground
            # we are rolling and decelerating
            if self.x_speed > 0:
                self.x_speed -= self.ground_friction * self.delta
                self.x_speed = max(self.x_speed, 0)
            if self.x_speed < 0:
                self.x_speed += self.ground_friction * self.delta
                self.x_speed = min(self.x_speed, 0)
            self.y_speed -= self.ground_friction * self.delta
            if self.y_speed < 0.001:
                # we slowed as much as we are not moving anymore
                self.y_speed = 0
                self.stop_update()
            speed_x = self.x_speed
            speed_y = self.y_speed
        else:
            # we are still in the air
            self.sprite.scale = self.evaluate_height() / 100.0 + 1
            speed_x = self.initial_speed[0]
            speed_y = self.initial_speed[1]

        new_x = self.sprite.x + speed_x * self.delta
        if new_x < TILE_SIZE or new_x > SCREEN_SIZE - TILE_SIZE - self.sprite.width:
            self.initial_speed[0] *= -1
            self.x_speed *= -1
            speed_x *= -1
            new_x = self.sprite.x + speed_x * self.delta
        self.sprite.x = new_x
        self.sprite.y += speed_y * self.delta

    def evaluate_height(self):
        end = self.impact_y - self.start_y
        end = max(1, end)
        current = self.sprite.y - self.start_y

        path_percent = current * 100. / end
        # parabolic function, maths FTW
        height_percent = 100 * (1 - ((path_percent - 50.) / 50.)**2)
        return height_percent  # for now
