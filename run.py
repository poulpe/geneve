#!/usr/bin/python3
# -*- coding: utf-8 -*-

import random
from medic import medics_batch
from enemy import Enemy
from tank import Tank
from power_cursor import PowerCursor
from counters import PointsCounter, LifeCounter
from constants import SCREEN_SIZE, MAX_TILES, TILE_SIZE, game_globals
from pyglet import window, app, graphics, image, sprite, clock, text

keys = window.key.KeyStateHandler()
win = window.Window(width=SCREEN_SIZE, height=SCREEN_SIZE)

sand_imgs = [image.load('data/sand_b11.png'),
             image.load('data/sand_b12.png'),
             image.load('data/sand_b13.png'),
             image.load('data/sand_b14.png'),
             image.load('data/sand_b21.png'),
             image.load('data/sand_b22.png'),
             image.load('data/sand_b23.png'),
             image.load('data/sand_b24.png'), ]
rock_img = image.load('data/rock1.png')
rock_img2 = image.load('data/rock2.png')
rock_img3 = image.load('data/rock3.png')
rock_img4 = image.load('data/rock4.png')

tank = Tank(keys, Enemy.active_enemies, Enemy.end_enemies)
power = PowerCursor(tank)
points = PointsCounter()
life = LifeCounter()
game_globals["points"] = points
game_globals["ended"] = False
enemies = []

enemies_batch = graphics.Batch()
background_batch = graphics.Batch()
background_sprites = []

for i in range(MAX_TILES):
    for j in range(MAX_TILES):
        if j is 0 or i in (0, MAX_TILES - 1):
            current_img = random.choice([rock_img,
                                         rock_img2,
                                         rock_img3,
                                         rock_img4, ])
        else:
            current_img = random.choice(sand_imgs)
        current_sprite = sprite.Sprite(current_img,
                                       batch=background_batch,
                                       x=i * TILE_SIZE,
                                       y=j * TILE_SIZE)
        background_sprites.append(current_sprite)


end_label = text.Label("",
                       font_name="Times New Roman",
                       font_size=32,
                       x=200,
                       y=368, )


def push_total():
    game_globals["ended"] = True
    end_label.text = "YOU KILLED %d MEDICS" % points.hits
    clock.schedule_once(lambda _delay: exit(), 4)


def spawn_enemy(_delta):
    enemies.append(Enemy(enemies_batch))
    # w/ 0pts/7sec between spawn till 20pts/2sec
    interval = 7.0 - .25 * points.hits
    clock.schedule_once(spawn_enemy, interval if interval > 2.0 else 2.0)
clock.schedule_once(spawn_enemy, 2)


def check_life(_delta):
    if not life.hit(len(Enemy.end_enemies)):
        clock.unschedule(spawn_enemy)
        clock.unschedule(check_life)
        clock.unschedule(tank.move)
        push_total()
clock.schedule_interval(check_life, 0.5)


@win.event
def on_draw():
    win.clear()
    background_batch.draw()
    power.draw()
    enemies_batch.draw()
    medics_batch.draw()
    tank.draw()
    points.draw()
    life.draw()
    if game_globals["ended"]:
        end_label.draw()


@win.event
def on_key_press(symbol, modifier):
    if game_globals["ended"]:
        return
    if symbol == window.key.UP:
        power.press()


@win.event
def on_key_release(symbol, modifier):
    if game_globals["ended"]:
        return
    if symbol == window.key.UP:
        power.release()

win.push_handlers(keys)
app.run()
