# -*- coding: utf-8 -*-

from constants import SCREEN_SIZE, TILE_SIZE
from movable import Movable
from rocket import Rocket
from pyglet import image, window, graphics

tank_img = image.load('data/tank.png')


class Tank(Movable):
    pos_y = 32
    pos_x = 511

    def __init__(self, keys, active_enemies, end_enemies):
        self.keys = keys
        self.speed = 0
        self.rockets = []
        self.rocket_batch = graphics.Batch()
        self.active_enemies = active_enemies
        self.end_enemies = end_enemies

        super(Tank, self).__init__(tank_img, self.move)

    def draw(self):
        super(Tank, self).draw()
        self.rocket_batch.draw()

    def move(self, _delta):
        accl = self.delta * 200

        if self.keys[window.key.LEFT]:
            self.speed -= accl
        if self.keys[window.key.RIGHT]:
            self.speed += accl
        if not self.keys[window.key.RIGHT] and not self.keys[window.key.LEFT]:
            self.speed *= 0.98

        self.speed = max(min(200, self.speed), -200)
        self.sprite.x += self.speed * self.delta

        if self.sprite.x < TILE_SIZE:
            self.sprite.x = TILE_SIZE
            self.speed = 0
        if self.sprite.x > SCREEN_SIZE - TILE_SIZE * 2:
            self.sprite.x = SCREEN_SIZE - TILE_SIZE * 2
            self.speed = 0

        for enemy in self.end_enemies[:]:
            if abs(enemy.sprite.x - self.sprite.x) < 16:
                self.end_enemies.remove(enemy)
                enemy.stunt_by = "toto"
                enemy.kill()

    def fire(self, power_amount=256):
        rocket = Rocket(self.sprite.x + 8,
                        self.rocket_batch,
                        [self.speed, power_amount],
                        self.active_enemies)
        self.rockets.append(rocket)
