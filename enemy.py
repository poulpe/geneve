# -*- coding: utf-8 -*-
from constants import TILE_SIZE, SCREEN_SIZE
from random import randint
from movable import Movable
from pyglet import image, sprite

from medic import call_medic, release_medic

img_fine = image.load('data/enemy_fine.png')
img_hurt = image.load('data/enemy_hurt.png')
img_dead = image.load('data/enemy_dead.png')

frames = ('data/enemy_fine.png',
          'data/enemy_fine2.png')
anim_fine = [image.load(img) for img in frames]


class Enemy(Movable):
    speed = -25
    active_enemies = []
    end_enemies = []
    stunt_by = None

    def __init__(self, batch):
        self.medics = []
        self.batch = batch
        # 16 is self.sprite.width but this is not initialized yet
        self.pos_x = randint(TILE_SIZE,
                             SCREEN_SIZE - TILE_SIZE - 16)
        # 22 is self.sprite.height but this is not initialized yet
        self.pos_y = SCREEN_SIZE + 22
        animation = image.Animation.from_image_sequence(anim_fine, 0.33)
        super(Enemy, self).__init__(animation,
                                    self.move,
                                    batch)
        Enemy.active_enemies.append(self)

    def move(self, _delta):
        if self.sprite.y < 32:
            Enemy.end_enemies.append(self)
            self.stop_update()
            return
        self.sprite.y += self.speed * self.delta

    def stun(self, rocket):
        if self.stunt_by:
            # a second stunt kills the enemy
            if self.stunt_by is not rocket:
                # check if it is not the same
                self.kill()
            return
        self.sprite.delete()
        self.sprite = sprite.Sprite(img_hurt,
                                    batch=self.batch,
                                    x=self.sprite.x,
                                    y=self.sprite.y)
        self.stunt_by = rocket
        self.stop_update()

        self.medics.append(call_medic(self))

    def heal(self):
        self.stunt_by = None
        animation = image.Animation.from_image_sequence(anim_fine, 0.33)
        self.pos_x = self.sprite.x
        self.pos_y = self.sprite.y
        self.sprite.delete()
        super(Enemy, self).__init__(animation,
                                    self.move,
                                    self.batch)

    def kill(self, _rocket=None):
        Enemy.active_enemies.remove(self)
        self.sprite.delete()
        self.sprite = sprite.Sprite(img_dead,
                                    batch=self.batch,
                                    x=self.sprite.x,
                                    y=self.sprite.y)
        if not self.stunt_by:
            # enemy has not been stunt, so we have to stop_update it
            self.stop_update()

        stunts = [enemy for enemy in Enemy.active_enemies if enemy.stunt_by]
        release_medic(self, stunts)
        self.medics = []
