# -*- coding: utf-8 -*-
from pyglet import sprite, clock


class Movable(object):
    pos_x = 0
    pos_y = 0
    delta = 1. / 60.

    def __init__(self, img, interval_cb, batch=None, group=None):
        self.sprite = sprite.Sprite(img,
                                    batch=batch,
                                    group=group,
                                    x=self.pos_x,
                                    y=self.pos_y)

        clock.schedule_interval(interval_cb, self.delta)
        self.cb = interval_cb

    def stop_update(self):
        clock.unschedule(self.cb)

    def draw(self):
        self.sprite.draw()
